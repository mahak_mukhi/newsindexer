package edu.buffalo.cse.irf14;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.buffalo.cse.irf14.index.DocAndTermMap;
import edu.buffalo.cse.irf14.index.IndexReader;
import edu.buffalo.cse.irf14.query.Clause;
import edu.buffalo.cse.irf14.query.Operator;
import edu.buffalo.cse.irf14.query.Query;


/**
 * Main class to run the searcher.
 * As before implement all TODO methods unless marked for bonus
 * @author nikhillo
 *
 */
public class SearchRunner {
	public enum ScoringModel {TFIDF, OKAPI};
	String indexDir;
	/**
	 * Default (and only public) constuctor
	 * @param indexDir : The directory where the index resides
	 * @param corpusDir : Directory where the (flattened) corpus resides
	 * @param mode : Mode, one of Q or E
	 * @param stream: Stream to write output to
	 */
	public SearchRunner(String indexDir, String corpusDir, 
			char mode, PrintStream stream) {
		//TODO: IMPLEMENT THIS METHOD
		this.indexDir = indexDir;
	}
	
	/**
	 * Method to execute given query in the Q mode
	 * @param userQuery : Query to be parsed and executed
	 * @param model : Scoring Model to use for ranking results
	 */
	public void query(String userQuery, ScoringModel model) {
		//TODO: IMPLEMENT THIS METHOD
	}
	
	/**
	 * Method to execute queries in E mode
	 * @param queryFile : The file from which queries are to be read and executed
	 */
	public void query(File queryFile) {
		//TODO: IMPLEMENT THIS METHOD
	}
	
	/**
	 * General cleanup method
	 */
	public void close() {
		//TODO : IMPLEMENT THIS METHOD
	}
	
	/**
	 * Method to indicate if wildcard queries are supported
	 * @return true if supported, false otherwise
	 */
	public static boolean wildcardSupported() {
		//TODO: CHANGE THIS TO TRUE ONLY IF WILDCARD BONUS ATTEMPTED
		return false;
	}
	
	/**
	 * Method to get substituted query terms for a given term with wildcards
	 * @return A Map containing the original query term as key and list of
	 * possible expansions as values if exist, null otherwise
	 */
	public Map<String, List<String>> getQueryTerms() {
		//TODO:IMPLEMENT THIS METHOD IFF WILDCARD BONUS ATTEMPTED
		return null;
		
	}
	
	/**
	 * Method to indicate if speel correct queries are supported
	 * @return true if supported, false otherwise
	 */
	public static boolean spellCorrectSupported() {
		//TODO: CHANGE THIS TO TRUE ONLY IF SPELLCHECK BONUS ATTEMPTED
		return false;
	}
	
	/**
	 * Method to get ordered "full query" substitutions for a given misspelt query
	 * @return : Ordered list of full corrections (null if none present) for the given query
	 */
	public List<String> getCorrections() {
		//TODO: IMPLEMENT THIS METHOD IFF SPELLCHECK EXECUTED
		return null;
	}
	
	public HashMap<DocAndTermMap,DocAndTermMap> processQuery(Query query)
	{
		if(query == null)
			return null;
		HashMap<DocAndTermMap,DocAndTermMap> resultSet = new HashMap<DocAndTermMap,DocAndTermMap>();
		//considering there will be no query like NOT apples AND oranges
		ArrayList<Clause> clauses = query.getClauses();
		ArrayList<Operator> operators = query.getOperators();
		resultSet = processClause(clauses.get(0));
		for(int i=1;i<clauses.size();i++)
		{
			HashMap<DocAndTermMap,DocAndTermMap> tempSet = processClause(clauses.get(i));
			Operator oper = operators.get(i-1);
			switch(oper){
			case AND:
				resultSet = intersect(resultSet,tempSet);
				break;
			case NOT:
				resultSet = minus(resultSet,tempSet);
				break;
			case OR:
				resultSet = union(resultSet,tempSet);
				break;
			}
		}
		return resultSet;
		
	}
	
	private HashMap<DocAndTermMap,DocAndTermMap> intersect(HashMap<DocAndTermMap,DocAndTermMap>
					setOne, HashMap<DocAndTermMap,DocAndTermMap> setTwo)
	{
		if(setOne == null || setTwo == null)
			return null;
		HashMap<DocAndTermMap,DocAndTermMap> iterSet = (setOne.size()<setTwo.size())?setOne:setTwo;
		HashMap<DocAndTermMap,DocAndTermMap> otherSet = (iterSet == setOne)? setTwo:setOne;
		HashMap<DocAndTermMap,DocAndTermMap> resultSet = new HashMap<DocAndTermMap,DocAndTermMap>(); 
		Iterator<Entry<DocAndTermMap,DocAndTermMap>> iter = iterSet.entrySet().iterator();
		while(iter.hasNext())
		{
			Entry<DocAndTermMap,DocAndTermMap> entry = iter.next();
			DocAndTermMap docAndTermMap = entry.getKey();
			if(otherSet.containsKey(docAndTermMap))
			{
				DocAndTermMap otherDocAndTermMap = otherSet.get(docAndTermMap);
				//merge the termMaps of both these docTermMaps;
				otherDocAndTermMap.getTermMap().putAll(docAndTermMap.getTermMap());
				resultSet.put(otherDocAndTermMap, otherDocAndTermMap);
			}
			
		}
		return resultSet;
	}
	
	private HashMap<DocAndTermMap,DocAndTermMap> union(HashMap<DocAndTermMap,DocAndTermMap> setOne,
			HashMap<DocAndTermMap,DocAndTermMap> setTwo){
		if(setOne == null || setTwo == null)
			return null;
		HashMap<DocAndTermMap,DocAndTermMap> iterSet = (setOne.size()<setTwo.size())?setOne:setTwo;
		HashMap<DocAndTermMap,DocAndTermMap> resultSet = (iterSet == setOne)? setTwo:setOne;
		//HashMap<DocAndTermMap,DocAndTermMap> resultSet = otherSet; 
		Iterator<Entry<DocAndTermMap,DocAndTermMap>> iter = iterSet.entrySet().iterator();
		while(iter.hasNext()){
			Entry<DocAndTermMap,DocAndTermMap> entry = iter.next();
			DocAndTermMap docAndTermMap = entry.getKey();
			if(resultSet.containsKey(docAndTermMap))
			{
				resultSet.get(docAndTermMap).getTermMap().putAll(docAndTermMap.getTermMap());
			}
			else{
				resultSet.put(docAndTermMap, docAndTermMap);
			}
		}
		return resultSet;
	}
	
	private HashMap<DocAndTermMap,DocAndTermMap> minus(HashMap<DocAndTermMap,DocAndTermMap> setOne,
			HashMap<DocAndTermMap,DocAndTermMap> setTwo){
		if(setOne == null || setTwo == null)
			return null;
		HashMap<DocAndTermMap,DocAndTermMap> iterSet = (setOne.size()<setTwo.size())?setOne:setTwo;
		HashMap<DocAndTermMap,DocAndTermMap> resultSet = (iterSet == setOne)? setTwo:setOne;
		//HashMap<DocAndTermMap,DocAndTermMap> resultSet = otherSet; 
		Iterator<Entry<DocAndTermMap,DocAndTermMap>> iter = iterSet.entrySet().iterator();
		while(iter.hasNext()){
			Entry<DocAndTermMap,DocAndTermMap> entry = iter.next();
			DocAndTermMap docAndTermMap = entry.getKey();
			if(resultSet.containsKey(docAndTermMap))
			{
				resultSet.remove(docAndTermMap);
			}
		}
		return resultSet;
	}
	
	public HashMap<DocAndTermMap,DocAndTermMap> processClause(Clause clause)
	{
		if(clause == null)
			return null;
		if(clause.getQuery() !=null){
			// get postings
			IndexReader reader = new IndexReader(indexDir,clause.getIndex());
			Map<String,Integer> postings = reader.getPostings(clause.getTerm());
			if(postings == null)
				return null;
			HashMap<DocAndTermMap,DocAndTermMap> docAndTermMapSet = new HashMap<DocAndTermMap
					,DocAndTermMap>();
			Iterator<Entry<String,Integer>> iter = postings.entrySet().iterator();
			while(iter.hasNext()){
				Entry<String,Integer> entry = iter.next();
				DocAndTermMap docAndTermMap = new DocAndTermMap(entry.getKey());
				docAndTermMap.getTermMap().put(clause.getTerm(), entry.getValue());
				docAndTermMapSet.put(docAndTermMap,docAndTermMap);
			}
			return docAndTermMapSet;
		}
		else{
			// call process query here
			return processQuery(clause.getQuery());
		}
	}
}
