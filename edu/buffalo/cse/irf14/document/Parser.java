/**
 * 
 */
package edu.buffalo.cse.irf14.document;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
//import java.util.LinkedList;
//import java.util.List;


/**
 * @author nikhillo
 * Class that parses a given file into a Document
 */
public class Parser {
	/**
	 * Static method to parse the given file into the Document object
	 * @param filename : The fully qualified filename to be parsed
	 * @return The parsed and fully loaded Document object
	 * @throws ParserException In case any error occurs during parsing
	 */
	public static Document parse(String filename) throws ParserException {
		// TODO YOU MUST IMPLEMENT THIS
		//System.out.println(filename);
		try{
			File file = new File(filename);
			
			Document document = new Document();
			String fileId = file.getName();
			document.setField(FieldNames.FILEID, new String[]{fileId});
			String category = file.getParentFile().getName();
			document.setField(FieldNames.CATEGORY, new String[]{category});
			BufferedReader br = new BufferedReader(
					new InputStreamReader(new FileInputStream(file)));
			boolean isTitleSet = false;
			boolean isAuthorSet = false;
			boolean arePlaceAndDateSet = false;
			String line = br.readLine();
			String content = "";
			while(line!=null)
			{
				if(line.trim().equals(""))
				{
					line = br.readLine();
					continue;
				}
				if(!isTitleSet) // to do : put all caps condition
				{
					String title = line;
					document.setField(FieldNames.TITLE, new String[]{title});
					isTitleSet = true;
					line = br.readLine();
					continue;
				}
				if(!isAuthorSet)
				{
					isAuthorSet = true;
					String authorAndOrgs[][] = getAuthorList(line);
					//System.out.println("Authors : ");
					//for(int i=0;i<authorAndOrgs[0].length;i++)
					//{
						//System.out.println(authorAndOrgs[0][i]);
					//}
					//System.out.println("Orgs : ");
					//for(int i=0;i<authorAndOrgs[1].length;i++)
					//{
						//System.out.println(authorAndOrgs[1][i]);
					//}
					if(authorAndOrgs!=null)
					{
						document.setField(FieldNames.AUTHOR, authorAndOrgs[0]);
						document.setField(FieldNames.AUTHORORG, authorAndOrgs[1]);
						line = br.readLine();
						continue;
					}
								
				}
				if(!arePlaceAndDateSet)
				{
					arePlaceAndDateSet = true;
					String values[] = line.split("-");
					String placeAndDate = values[0].trim();
					//String date = values[1].trim().substring(0, 7);
					if(values.length>1){
						content = values[1].trim();
					}
					values = placeAndDate.split(",");
					String date="";
					String place="";
					if(values.length>0){
						date = values[values.length-1].trim();
						place=values[0].trim();
						for(int i=1;i<values.length-1;i++)
						{
							place = place + ", " + values[i].trim();
						}
					}
					document.setField(FieldNames.PLACE, new String[]{place});
					document.setField(FieldNames.NEWSDATE, new String[]{date});
					//System.out.println("place : " + place);
					//System.out.println("date : " + date);
					//System.out.println("content : " + content);
					line = br.readLine();
					continue;
				}
				//System.out.println(line);
				//if(line.contains("AUTHOR")){
					//System.out.println(file.getAbsolutePath()+" " + 
						//	file.getName());
				//}
				//System.out.println("--------");
				content = content + " " +line ;
				line = br.readLine();
				
			}
			br.close();
			//System.out.println(content);
			document.setField(FieldNames.CONTENT, new String[]{content});
			if(!isTitleSet)
			{
				throw new ParserException();
			}
			
//			System.out.println(document.getField(FieldNames.AUTHOR)[0]);
//			System.out.println(document.getField(FieldNames.AUTHORORG)[0]);
//			System.out.println(document.getField(FieldNames.CATEGORY)[0]);
//			System.out.println(document.getField(FieldNames.CONTENT)[0]);
//			System.out.println(document.getField(FieldNames.FILEID)[0]);
//			System.out.println(document.getField(FieldNames.NEWSDATE)[0]);
//			System.out.println(document.getField(FieldNames.PLACE)[0]);
//			System.out.println(document.getField(FieldNames.TITLE)[0]);
			return document;
		}
		catch(Exception e){
			//e.printStackTrace();
			//System.out.println(filename);
			throw new ParserException();
		}
		
	}
	
	static String[][] getAuthorList(String line)
	{
		if(!line.toUpperCase().contains("<AUTHOR>"))
		{
			return null;
		}
		String authorList=line.split("(?i)<author>")[1];
		authorList = authorList.split("(?i)</author>")[0];
		authorList = authorList.trim();
		String authorsAndOrgs[] = authorList.split("(?i)and");
		int length = authorsAndOrgs.length;
		String authors[] = new String[length];
		String orgs[] = new String[length];
		for(int i=0;i<authorsAndOrgs.length;i++)
		{
			String values[] = authorsAndOrgs[i].split(",");
			if(i==0){
				authors[i] = values[0].trim().substring(2).trim();
			}else{
				authors[i] = values[0].trim();
			}
			// ponder over it - should it be null or ""?
			if(values.length >1){
				orgs[i] = values[1].trim();
			}
			else{
				orgs[i]="";
			}
			
			
		}
		return new String[][]{authors,orgs};
		//System.out.println(authorList);
		
		//return authorsAndOrgs;
	}

}
