/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author nikhillo
 * Class that represents a stream of Tokens. All {@link Analyzer} and
 * {@link TokenFilter} instances operate on this to implement their
 * behavior
 */
public class TokenStream implements Iterator<Token>{
	
	private List<Token> theStream = new LinkedList<Token>();
	//private int pointer=0;
	private int pointer=-1;
	private int removeIndex = -1;
	public TokenStream(String strings[])
	{
		if(strings.length>0)
		{
			boolean firstWord = true;
			for(int i=0;i<strings.length;i++)
			{
				if(strings[i] == null)
					continue;
				String tokenString = strings[i].trim();
				if(tokenString.equals(""))
					continue;
				Token token = new Token(tokenString);
				// check if it contains atleast one alphanumeric character
				
				if(!tokenString.matches("[^a-zA-Z0-9]*")){
					if(firstWord){
						token.setFirst(true);
						firstWord = false;
					}
					if(i==strings.length-1){
						token.setLast(true);
						
					}else{
						// check if this is the last word
						// 1. if this word contains a . ? ! or a trail
						// of these in the end, or
						// 2. the next word contains a full stop in the begining
						// will be removing the 2nd condition, since the test
						// cases have something else to say
						// so instead we'll see if the next token is 
						// just . or ? or !
						//char charAtEndOfCurrentToken = tokenString.charAt(tokenString.length()-1);
						//char charAtBegOfNextToken = strings[i+1].charAt(0);
						String regexForTrailingSymbols = "^.+[.?!]+[']?$";
						String regextForAllSymbols = "^[.?!]+[']?$";
						String nextTokenString = strings[i+1].trim();
						//if((charAtEndOfCurrentToken == '.')||
							//	(charAtEndOfCurrentToken == '?')||
								//(charAtEndOfCurrentToken == '!')||
								//(charAtBegOfNextToken == '.')||
								//(charAtBegOfNextToken == '?')||
								//(charAtBegOfNextToken == '!'))
								//(nextToken.equals("."))||
								//(nextToken.equals("?"))||
								//(nextToken.equals("!")))
						if(tokenString.matches(regexForTrailingSymbols) ||
								nextTokenString.matches(regextForAllSymbols))
						{
							token.setLast(true);
							firstWord = true;
						}
					}
				}
				
				theStream.add(token);
			}
		}
	}
	
	/**
	 * Method that checks if there is any Token left in the stream
	 * with regards to the current pointer.
	 * DOES NOT ADVANCE THE POINTER
	 * @return true if at least one Token exists, false otherwise
	 */
	@Override
	public boolean hasNext() {
		// TODO YOU MUST IMPLEMENT THIS
		if(theStream.size()==0){
			removeIndex = -1;
			return false;
		}
		if(pointer == (theStream.size()-1))
		{
			//removeIndex=-1;
			return false;
		}
		else
			return true;
	}

	/**
	 * Method to return the next Token in the stream. If a previous
	 * hasNext() call returned true, this method must return a non-null
	 * Token.
	 * If for any reason, it is called at the end of the stream, when all
	 * tokens have already been iterated, return null
	 */
	@Override
	public Token next() {
		// TODO YOU MUST IMPLEMENT THIS
		if(!hasNext())
		{
			removeIndex=-1;
			return null;
		}
		else
		{
			pointer++;
			//removeIndex = pointer-1;
			removeIndex = pointer;
			return theStream.get(removeIndex);
		}
	}
	
	/**
	 * Method to remove the current Token from the stream.
	 * Note that "current" token refers to the Token just returned
	 * by the next method. 
	 * Must thus be NO-OP when at the beginning of the stream or at the end
	 */
	@Override
	public void remove() {
		// TODO YOU MUST IMPLEMENT THIS
		if(removeIndex!=-1)
		{
			theStream.remove(removeIndex);
			pointer--;
			removeIndex=-1;
		}
		
	}
	
	/**
	 * Method to reset the stream to bring the iterator back to the beginning
	 * of the stream. Unless the stream has no tokens, hasNext() after calling
	 * reset() must always return true.
	 */
	public void reset() {
		//TODO : YOU MUST IMPLEMENT THIS
		pointer=-1;
		removeIndex=-1;
	}
	
	/**
	 * Method to append the given TokenStream to the end of the current stream
	 * The append must always occur at the end irrespective of where the iterator
	 * currently stands. After appending, the iterator position must be unchanged
	 * Of course this means if the iterator was at the end of the stream and a 
	 * new stream was appended, the iterator hasn't moved but that is no longer
	 * the end of the stream.
	 * @param stream : The stream to be appended
	 */
	public void append(TokenStream stream) {
		//TODO : YOU MUST IMPLEMENT THIS
		if(stream!=null)
		{
			stream.reset();
			while(stream.hasNext())
			{
				theStream.add(stream.next());
			}
		}
	}
	
	/**
	 * Method to get the current Token from the stream without iteration.
	 * The only difference between this method and {@link TokenStream#next()} is that
	 * the latter moves the stream forward, this one does not.
	 * Calling this method multiple times would not alter the return value of {@link TokenStream#hasNext()}
	 * @return The current {@link Token} if one exists, null if end of stream
	 * has been reached or the current Token was removed
	 */
	public Token getCurrent() {
		//TODO: YOU MUST IMPLEMENT THIS
		if(pointer>=0 && pointer<theStream.size())
		{
			if(removeIndex==-1)
				return null;
			
			//Token token = theStream.get(pointer);
			//removeIndex = pointer;
			return theStream.get(pointer);
		}
		return null;
	}
	
	public void insert(Token token)
	{
		if(pointer<0 || pointer>theStream.size())
		{
			return;
		}
		theStream.add(pointer+1, token);
		
	}
	
	public String tokenCat(int number)
	{
		if(pointer<0 || pointer>=theStream.size())
			return null;
		if(number<=0)
			return "";
		String concatString = theStream.get(pointer).toString();
		for(int i=pointer+1;i<(pointer+number);i++)
		{
			if(i>=theStream.size())
				break;
			concatString = concatString + " " + theStream.get(i);
		}
		return concatString;
	}
	
	public Token nthTokenToRight(int n)
	{
		if(pointer<0 || pointer+n>=theStream.size())
			return null;
		return theStream.get(pointer+n);
	}
	
}
