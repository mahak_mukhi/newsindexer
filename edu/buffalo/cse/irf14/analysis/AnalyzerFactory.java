/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;

import edu.buffalo.cse.irf14.document.FieldNames;

/**
 * @author nikhillo
 * This factory class is responsible for instantiating "chained" {@link Analyzer} instances
 */
public class AnalyzerFactory {
	// this implementation isn't thread safe
	
	private static AnalyzerFactory analyzerFactory;
	
	private AnalyzerFactory(){}
	/**
	 * Static method to return an instance of the factory class.
	 * Usually factory classes are defined as singletons, i.e. 
	 * only one instance of the class exists at any instance.
	 * This is usually achieved by defining a private static instance
	 * that is initialized by the "private" constructor.
	 * On the method being called, you return the static instance.
	 * This allows you to reuse expensive objects that you may create
	 * during instantiation
	 * @return An instance of the factory
	 */
	public static AnalyzerFactory getInstance() {
		//TODO: YOU NEED TO IMPLEMENT THIS METHOD
		if(analyzerFactory!=null)
			return analyzerFactory;
		analyzerFactory = new AnalyzerFactory();
		return analyzerFactory;
	}
	
	/**
	 * Returns a fully constructed and chained {@link Analyzer} instance
	 * for a given {@link FieldNames} field
	 * Note again that the singleton factory instance allows you to reuse
	 * {@link TokenFilter} instances if need be
	 * @param name: The {@link FieldNames} for which the {@link Analyzer}
	 * is requested
	 * @param TokenStream : Stream for which the Analyzer is requested
	 * @return The built {@link Analyzer} instance for an indexable {@link FieldNames}
	 * null otherwise
	 * @throws TokenizerException 
	 */
	public Analyzer getAnalyzerForField(FieldNames name, TokenStream stream) throws TokenizerException {
		//TODO : YOU NEED TO IMPLEMENT THIS METHOD
		TokenFilterFactory filterFactory = TokenFilterFactory.getInstance();
		TokenFilter filter;
		// we apply a filter to the whole stream and then the
		// next and so forth
		if(name == null)
			return null;
		
		switch(name){
		case AUTHOR:
			filter = filterFactory.getFilterByType(TokenFilterType.CAPITALIZATION, stream);
			while(filter.increment());
			return filter;
		case AUTHORORG:
			filter = filterFactory.getFilterByType(TokenFilterType.SYMBOL, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.CAPITALIZATION, stream);
			while(filter.increment());
			return filter;
			
		case CATEGORY:
			filter = filterFactory.getFilterByType(TokenFilterType.CAPITALIZATION, stream);
			while(filter.increment());
			return filter;			
			
		case TITLE:
			
		case CONTENT:
			filter = filterFactory.getFilterByType(TokenFilterType.DATE, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			//filter = filterFactory.getFilterByType(TokenFilterType.STOPWORD, stream);
			//while(filter.increment());
			//stream = filter.getStream();
			//stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.SYMBOL, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.ACCENT, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.SPECIALCHARS, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			//filter = filterFactory.getFilterByType(TokenFilterType.DATE, filter.getStream());
			//while(filter.increment());
			filter = filterFactory.getFilterByType(TokenFilterType.NUMERIC, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.CAPITALIZATION, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.STEMMER, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.STOPWORD, stream);
			while(filter.increment());
			return filter;
			//break;
		//case FILEID:
			//we need a fake filter
			//filter = filterFactory.getFilterByType(TokenFilterType.NOFILTER, stream);
			//while(filter.increment());
			//return filter;
			
		case NEWSDATE:
			filter = filterFactory.getFilterByType(TokenFilterType.DATE, stream);
			while(filter.increment());
			return filter;
		case PLACE:
			filter = filterFactory.getFilterByType(TokenFilterType.SPECIALCHARS, stream);
			while(filter.increment());
			stream = filter.getStream();
			stream.reset();
			filter = filterFactory.getFilterByType(TokenFilterType.CAPITALIZATION, stream);
			while(filter.increment());
			return filter;
		
		default:
			return null;
		
		}
				
	}
	
}
