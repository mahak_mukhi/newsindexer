/**
 * 
 */
package edu.buffalo.cse.irf14.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
//import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
//import java.util.Map.Entry;
//import java.util.PriorityQueue;
import java.util.TreeSet;



/**
 * @author nikhillo
 * Class that emulates reading data back from a written index
 */
public class IndexReader {
	
	private class TermAndFreq implements Comparable<TermAndFreq>{
		String term;
		int freq;
		public TermAndFreq(String term,int freq)
		{
			this.term = term;
			this.freq = freq;
		}
		@Override
		public int compareTo(TermAndFreq o) {
			// TODO Auto-generated method stub
			if(o==null)
				return 1;
			return freq-o.freq;
			//return 0;
		}
		
		
	}

	
	private final static String DOC_DICT = "doc_dict";
	private final static String NON_ALPHA_FILE = "non_alpha";
	private final static String[] FOLDER_NAMES = {"category","term","author","place","dictionary"};
	private final static int BUCKET_CAP = 27;
	private String rootDirectory;
	//private IndexType indexType;
	private String subDir;
	/**
	 * Default constructor
	 * @param indexDir : The root directory from which the index is to be read.
	 * This will be exactly the same directory as passed on IndexWriter. In case 
	 * you make subdirectories etc., you will have to handle it accordingly.
	 * @param type The {@link IndexType} to read from
	 */
	public IndexReader(String indexDir, IndexType type) {
		//TODO
		rootDirectory = indexDir;
		//this.indexType = type;
		switch(type){
		case AUTHOR:
			subDir = "author";
			break;
		case CATEGORY:
			subDir = "category";
			break;
		case PLACE:
			subDir = "place";
			break;
		case TERM:
			subDir = "term";
			break;
		}
	}
	/**
	 * Get total number of terms from the "key" dictionary associated with this 
	 * index. A postings list is always created against the "key" dictionary
	 * @return The total number of terms
	 */
	public int getTotalKeyTerms() {
		//TODO : YOU MUST IMPLEMENT THIS
		int totalCount = 0;
		for(int index=0;index<BUCKET_CAP;index++){
			String fileName;
			if(index<BUCKET_CAP-1)
			{
				fileName = Character.valueOf((char) (index + 'a')).toString();
			}
			else
			{
				fileName = NON_ALPHA_FILE; 
			}
			File file = new File(rootDirectory + File.separator + 
					subDir + File.separator + fileName);
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						new FileInputStream(file)));
				String value;
				while((value = reader.readLine())!=null)
				{
					if(!value.equals(""))
					{
						totalCount ++;
						reader.readLine();
					}
				}
				reader.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -1;
			}
		}
		return totalCount;
		//return -1;
	}
	
	/**
	 * Get total number of terms from the "value" dictionary associated with this 
	 * index. A postings list is always created with the "value" dictionary
	 * @return The total number of terms
	 */
	public int getTotalValueTerms() {
		//TODO: YOU MUST IMPLEMENT THIS
		File file = new File(rootDirectory + File.separator + 
				FOLDER_NAMES[4] + File.separator + DOC_DICT);
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader
					(new FileInputStream(file)));
			String value = br.readLine();
			br.close();
			if(value==null || value.equals(""))
			{
				return 0;
			}
			return Integer.parseInt(value);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		
		
		//return -1;
	}
	
	/**
	 * Method to get the postings for a given term. You can assume that
	 * the raw string that is used to query would be passed through the same
	 * Analyzer as the original field would have been.
	 * @param term : The "analyzed" term to get postings for
	 * @return A Map containing the corresponding fileid as the key and the 
	 * number of occurrences as values if the given term was found, null otherwise.
	 */
	public Map<String, Integer> getPostings(String term) {
		//TODO:YOU MUST IMPLEMENT THIS
		if(term==null||term.equals(""))
		{
			return null;
		}
		String fileName;
		if(term.matches("^[a-zA-Z].*$")){
			fileName = String.valueOf(term.charAt(0)).toLowerCase();
		}
		else
		{
			fileName = NON_ALPHA_FILE;
		}
		File file = new File(rootDirectory + File.separator + 
				subDir + File.separator + fileName);
		//if(!file.exists())
			
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(file)));
			
			HashMap<String,Integer> map = new HashMap<String,Integer>();
			boolean found = false;
			String line;
			while((line=reader.readLine())!=null)
			{
				if(line.equals(term)){
					//System.out.println(line);
					found = true;
					String postings = reader.readLine();
					if(postings!=null)
					{
						String filesndFreqs[] = postings.split(",");
						for(String fileAndFreq : filesndFreqs)
						{
							String indvVal[] = fileAndFreq.split("-");
							String fileId = indvVal[0];
							Integer freq = Integer.parseInt(indvVal[1]);
							map.put(fileId, freq);
						}
					}
					break;
				}
			}
			reader.close();
			if(!found)
			{
				return null;
			}
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method to get the top k terms from the index in terms of the total number
	 * of occurrences.
	 * @param k : The number of terms to fetch
	 * @return : An ordered list of results. Must be <=k fr valid k values
	 * null for invalid k values
	 */
	public List<String> getTopK(int k) {
		//TODO YOU MUST IMPLEMENT THIS
		if(k<=0)
			return null;
		
		TreeSet<TermAndFreq> sortedSet = new TreeSet<TermAndFreq>();
		//PriorityQueue<TermAndFreq> sortedSet = new PriorityQueue<IndexReader.TermAndFreq>();
		for(int i=0;i<BUCKET_CAP;i++){
			String fileName;
			if(i<BUCKET_CAP-1){
				fileName = Character.valueOf((char)(i + 'a')).toString();
			}
			else
			{
				fileName = NON_ALPHA_FILE;
			}
			File file =new File(rootDirectory + File.separator + 
					subDir + File.separator + fileName);
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						new FileInputStream(file)));
				String line;
				while((line=reader.readLine())!=null)
				{
					String term = line;
					int freq = 0;
					String fileAndFreq = reader.readLine();
					if(fileAndFreq!=null&&!fileAndFreq.equals(""))
					{
						String[] sepratedValues = fileAndFreq.split(",");
						//int freq = 0;
						for(String value : sepratedValues)
						{
							freq+= Integer.parseInt(value.split("-")[1]);
						}
					}
					TermAndFreq termAndFreq = new TermAndFreq(term,freq);
					// add to tree set
					if(sortedSet.size()<k)
					{
						sortedSet.add(termAndFreq);
						
					}
					else
					{
						
						TermAndFreq smallest = sortedSet.first();
						//obselete.term;
						if(termAndFreq.compareTo(smallest)>0)
						{
							sortedSet.remove(smallest);
							sortedSet.add(termAndFreq);
						}
						
					
						//sortedSet.add(termAndFreq);
					}
				}
				reader.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ArrayList<String> toReturn = new ArrayList<String>();
		Iterator<TermAndFreq> iter = sortedSet.descendingIterator();
		while(iter.hasNext()){
			toReturn.add(iter.next().term);
		}
		return toReturn;
	}
	
	/**
	 * Method to implement a simple boolean AND query on the given index
	 * @param terms The ordered set of terms to AND, similar to getPostings()
	 * the terms would be passed through the necessary Analyzer.
	 * @return A Map (if all terms are found) containing FileId as the key 
	 * and number of occurrences as the value, the number of occurrences 
	 * would be the sum of occurrences for each participating term. return null
	 * if the given term list returns no results
	 * BONUS ONLY
	 */
	public Map<String, Integer> query(String...terms) {
		//TODO : BONUS ONLY
		HashMap<String,Integer> map = null;
		if(terms == null || terms.length==0)
		{
			return null;
		}
		for(String term : terms){
			Map<String,Integer> termMap = getPostings(term);
			if(termMap==null)
				return null;
			if(map==null)
			{
				map = (HashMap<String, Integer>) termMap;
				continue;
			}
			HashMap<String,Integer> tempMap = new HashMap<String,Integer>();
			Set<Entry<String,Integer>> termMapSet = termMap.entrySet();
			Iterator<Entry<String,Integer>> iter = termMapSet.iterator();
			boolean doesIntersect = false;
			while(iter.hasNext()){
				Entry<String,Integer> entry = iter.next();				
				
				if(map.containsKey(entry.getKey()))
				{
					doesIntersect = true;
					int count = entry.getValue() + map.get(entry.getKey());
					tempMap.put(entry.getKey(), count);
				}
				
			}
			if(doesIntersect)
			{
				map = tempMap;
			}
			else
			{
				return null;
			}
		}
		// sort the map here
		TreeSet<TermAndFreq> treeSet = new TreeSet<TermAndFreq>();
		Set<Entry<String,Integer>> mapSet = map.entrySet();
		Iterator<Entry<String,Integer>> iter = mapSet.iterator();
		while(iter.hasNext()){
			Entry<String,Integer> entry = iter.next();
			treeSet.add(new TermAndFreq(entry.getKey(),entry.getValue()));
		}
		map = new LinkedHashMap<String,Integer>();
		Iterator<TermAndFreq> iterator = treeSet.descendingIterator();
		while(iterator.hasNext()){
			TermAndFreq termAndFreq = iterator.next();
			map.put(termAndFreq.term, termAndFreq.freq);
		}
		System.out.println(map);
		return map;
	}
}
