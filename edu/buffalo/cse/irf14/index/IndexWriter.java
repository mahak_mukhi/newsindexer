/**
 * 
 */
package edu.buffalo.cse.irf14.index;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
//import java.util.concurrent.locks.ReentrantLock;

import edu.buffalo.cse.irf14.analysis.AnalyzerFactory;
//import edu.buffalo.cse.irf14.analysis.Token;
import edu.buffalo.cse.irf14.analysis.TokenStream;
import edu.buffalo.cse.irf14.analysis.Tokenizer;
//import edu.buffalo.cse.irf14.analysis.TokenizerException;
import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;

/**
 * @author nikhillo
 * Class responsible for writing indexes to disk
 */
public class IndexWriter {
	private static class IndexMap extends ReentrantLock
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		HashMap<String,HashMap<String,Integer>> map = new HashMap<String,HashMap<String,Integer>>();		
	}
	private static class Dictionary extends ReentrantLock
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 2L;
		HashSet<String> set = new HashSet<String>();
	}
	private static class WritingThread implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!stopThread)
			{
				processMaps();
				writeDocumentDictionary();
				//writeTermDictionary();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
			processMaps();
			writeDocumentDictionary();
			//writeTermDictionary();
		}
		
	}
	private static volatile boolean stopThread = false;
	private final static int BUCKET_CAP = 27;
	volatile private static IndexMap categoryMap[];
	volatile private static IndexMap authorMap[];
	volatile private static IndexMap termMap[];
	volatile private static IndexMap placeMap[];
	volatile private static Dictionary documentDictionary;
	volatile private static Dictionary termDictionary[];
	//volatile private static int writingInstances;
	private final static String DOC_DICT = "doc_dict";
	private final static String NON_ALPHA_FILE = "non_alpha";
	private final static String[] FOLDER_NAMES = {"category","term","author","place","dictionary"};
	//private int docCount = 0;
	private static String rootDirectory;
	private static Thread writeThread = null;
	
	
	static{
		categoryMap = new IndexMap[BUCKET_CAP];
		authorMap = new IndexMap[BUCKET_CAP];
		termMap = new IndexMap[BUCKET_CAP];
		placeMap = new IndexMap[BUCKET_CAP];
		documentDictionary = new Dictionary();
		termDictionary = new Dictionary[BUCKET_CAP];
		for(int i=0;i<BUCKET_CAP;i++)
		{
			categoryMap[i] = new IndexMap();
			authorMap[i] = new IndexMap();
			termMap[i] = new IndexMap();
			placeMap[i] = new IndexMap();
			termDictionary[i] = new Dictionary();
		}
		//writingInstances = 0;
	}
	
	/**
	 * Default constructor
	 * @param indexDir : The root directory to be used for indexing
	 */
	public IndexWriter(String indexDir) {
		//TODO : YOU MUST IMPLEMENT THIS
		
		
		rootDirectory = indexDir;
		File dir = new File(rootDirectory);
		if(!dir.exists() || !dir.isDirectory())
		{
			dir.mkdir();
		}
		else{
			
			cleanDirectory(dir);
		}
		for(int i=0;i<FOLDER_NAMES.length;i++)
		{
			dir = new File(rootDirectory + File.separator + FOLDER_NAMES[i]);
			if(!dir.exists() || !dir.isDirectory())
			{
				dir.mkdir();
			}
		}
	}
	private void cleanDirectory(File dir)
	{
		File files[] = dir.listFiles();
		if(files == null || files.length<=0)
			return ;
		for(File file : files)
		{
			if(file.isFile())
				file.delete();
			else
				removeDirectory(file);
		}
	}
	private void removeDirectory(File dir){
		File files[] = dir.listFiles();
		if(files == null || files.length<=0){
			dir.delete();
			return ;
		}
		for(File file : files){
			if(file.isFile())
				file.delete();
			else
				removeDirectory(file);
		}
		dir.delete();
	}
	/**
	 * Method to add the given Document to the index
	 * This method should take care of reading the filed values, passing
	 * them through corresponding analyzers and then indexing the results
	 * for each indexable field within the document. 
	 * @param d : The Document to be added
	 * @throws IndexerException : In case any error occurs
	 */
	public void addDocument(Document d) throws IndexerException {
		//TODO : YOU MUST IMPLEMENT THIS
		//synchronized(LOCK){
			if(writeThread==null)
			{
				writeThread = new Thread(new WritingThread());
				writeThread.start();
			}
			//if(docCount>=1000)
			//{
				//docCount = 0;
				//processMaps();
				//writeDocumentDictionary();
				//writeTermDictionary();
			//}
			//System.out.println(d);
			AnalyzerFactory analyzerFactory = AnalyzerFactory.getInstance();
			String fileId = d.getField(FieldNames.FILEID)[0];
			Tokenizer tokenizer = new Tokenizer();
			TokenStream tokenStream;
			try {
				String parsedStrings[] = d.getField(FieldNames.CONTENT);
				
				if(parsedStrings!=null && parsedStrings[0].length()!=0){
					tokenStream = tokenizer.consume(parsedStrings[0]);
					tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.CONTENT,
							tokenStream).getStream();
					tokenStream.reset();
					mapStream(tokenStream, IndexType.TERM, fileId);
				}
				parsedStrings = d.getField(FieldNames.TITLE);
				
				if(parsedStrings!=null && parsedStrings[0].length()!=0)
				{
					tokenStream = tokenizer.consume(parsedStrings[0]);
					tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.TITLE,
							tokenStream).getStream();
					tokenStream.reset();
					mapStream(tokenStream,IndexType.TERM,fileId);
				}
				parsedStrings = d.getField(FieldNames.AUTHORORG);
				
				if(parsedStrings!=null)
				{
					for(int i=0;i<parsedStrings.length;i++){
						if(parsedStrings[i].length()!=0){
							tokenStream = tokenizer.consume(parsedStrings[i]);
							tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.AUTHORORG,
									tokenStream).getStream();
							tokenStream.reset();
							mapStream(tokenStream,IndexType.TERM,fileId);
						}
					}
				}
				parsedStrings = d.getField(FieldNames.NEWSDATE);
				if(parsedStrings!=null && parsedStrings[0].length()!=0){
					tokenStream = tokenizer.consume(parsedStrings[0]);
					tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.NEWSDATE,
							tokenStream).getStream();
					tokenStream.reset();
					mapStream(tokenStream,IndexType.TERM,fileId);
				}
				parsedStrings  = d.getField(FieldNames.CATEGORY);
				if(parsedStrings!=null && parsedStrings[0].length()!=0)
				{
					tokenStream = tokenizer.consume(parsedStrings[0]);
					tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.CATEGORY,
							tokenStream).getStream();
					tokenStream.reset();
					mapStream(tokenStream,IndexType.CATEGORY,fileId);
				}
				parsedStrings = d.getField(FieldNames.AUTHOR);
				if(parsedStrings!=null){
					for(int i=0;i<parsedStrings.length;i++)
					{
						if(parsedStrings[i].length()!=0){
							tokenStream = tokenizer.consume(parsedStrings[i]);
							tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.AUTHOR,
									tokenStream).getStream();
							tokenStream.reset();
							mapStream(tokenStream,IndexType.AUTHOR,fileId);
						}
					}
				}
				parsedStrings = d.getField(FieldNames.PLACE);
				if(parsedStrings!=null && parsedStrings[0].length()!=0){
					for(int i=0;i<parsedStrings.length;i++){
						tokenStream = tokenizer.consume(parsedStrings[i]);
						tokenStream = analyzerFactory.getAnalyzerForField(FieldNames.PLACE, 
								tokenStream).getStream();
						tokenStream.reset();
						mapStream(tokenStream,IndexType.PLACE,fileId);
					}
				}
				documentDictionary.lock();
				//System.out.println("acquired");
				documentDictionary.set.add(fileId);
				documentDictionary.unlock();
				//docCount++;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new IndexerException();
			}
		//}
	}
	
	private void mapStream(TokenStream stream,IndexType type,String value)
	{
		//synchronized (LOCK) {
			IndexMap map[];
			switch (type) {
			case AUTHOR:
				map = authorMap;
				break;
			case CATEGORY:
				map = categoryMap;
				break;
			case PLACE:
				map = placeMap;
				break;
			case TERM:
				map = termMap;
				break;
			default:
				return;

			}
			
			while(stream.hasNext()){
				//System.out.println("here");
				String key = stream.next().toString();
				if(key == null || key.trim().equals(""))
					continue;
				int index;
				if(key.matches("^[a-zA-Z].*$"))
				{
					index = Character.toLowerCase(key.charAt(0)) - 'a';
				}
				else
				{
					index = BUCKET_CAP-1;
				}
				//termDictionary[index].lock();
				//termDictionary[index].set.add(key);
				//termDictionary[index].unlock();
				map[index].lock();
				HashMap<String,Integer> values = map[index].map.get(key);
				if(values == null)
					values = new HashMap<String,Integer>();
				int count = 0;
				if(values.containsKey(value)){
					count = values.get(value);
				}
				//System.out.println("the count : " + count);
				count++;
				values.put(value,count);
				map[index].map.put(key, values);
				map[index].unlock();
			}
		}
	//}
	
	/**
	 * Method that indicates that all open resources must be closed
	 * and cleaned and that the entire indexing operation has been completed.
	 * @throws IndexerException : In case any error occurs
	 */
	public void close() throws IndexerException {
		//TODO
		//System.out.println("started writing");
		//processMaps();
		//writeDocumentDictionary();
		//processMaps();
		//writeDocumentDictionary();
		//writeTermDictionary();
		stopThread = true;
		try {
			writeThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	public static void writeDocumentDictionary()
	{
		//synchronizer(LOCK){
			documentDictionary.lock();
			//System.out.println("acquired");
			HashSet<String> dictBuffer = documentDictionary.set;
			documentDictionary.set = new HashSet<String>();
			documentDictionary.unlock();
			//System.out.println("left");
			File file = new File(rootDirectory+ File.separator + 
					FOLDER_NAMES[4] + File.separator + DOC_DICT);
			
			try {
				if(file.exists())
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader
							(new FileInputStream(file)));
					reader.readLine();
					String line;
					while((line = reader.readLine())!=null){
						if(line.equals(""))
							continue;
						dictBuffer.add(line);
					}
					reader.close();
				}
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(file)));
				bw.append(String.valueOf(dictBuffer.size()));
				bw.newLine();
				Iterator<String> iter = dictBuffer.iterator();
				while(iter.hasNext()){
					bw.append(iter.next());
					bw.newLine();
					//iter.remove();
				}
				bw.close();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		//}
	}
	
	public static void writeTermDictionary(){
		
		for(int index=0;index<BUCKET_CAP;index++){
			//termDictionary[index].lock();
			HashSet<String> termBuffer = termDictionary[index].set;
			termDictionary[index].set = new HashSet<String>();
			//termDictionary[index].unlock();
			String fileName;
			if(index<BUCKET_CAP-1)
			{
				fileName = Character.valueOf((char) (index + 'a')).toString();
			}
			else
			{
				fileName = NON_ALPHA_FILE; 
			}
			File file = new File(rootDirectory+ File.separator + 
					FOLDER_NAMES[4] + File.separator + fileName);
			try
			{
				if(file.exists())
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(
							new FileInputStream(file)));
					reader.readLine();
					String line;
					while((line=reader.readLine())!=null){
						if(line.equals(""))
							continue;
						termBuffer.add(line);
					}
					reader.close();
				}
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(file)));
				writer.append(String.valueOf(termBuffer.size()));
				writer.newLine();
				Iterator<String> iter = termBuffer.iterator();
				while(iter.hasNext()){
					writer.append(iter.next());
					writer.newLine();
				}
				writer.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
		
 
	
	public static void processMaps()
	{
		//synchronized(LOCK){
			writeOnDisk(categoryMap,"category");
			//System.out.println("processesd category");
			writeOnDisk(authorMap,"author");
			//System.out.println("processed author");
			writeOnDisk(termMap,"term");
			//System.out.println("processed term");
			writeOnDisk(placeMap,"place");
			//System.out.println("processed place");
		//}
	}
	private static void writeOnDisk(IndexMap[] map, String dirName)
	{
		//synchronized(LOCK){
			for(int mapIndex = 0; mapIndex<BUCKET_CAP;mapIndex++){
				map[mapIndex].lock();
				HashMap<String,HashMap<String,Integer>> bucket = map[mapIndex].map;
				map[mapIndex].map = new HashMap<String,HashMap<String,Integer>>();
				map[mapIndex].unlock();
				String fileName;
				if(mapIndex<BUCKET_CAP-1)
				{
					fileName = Character.valueOf((char) (mapIndex + 'a')).toString();
				}
				else
				{
					fileName = NON_ALPHA_FILE; 
				}
				File file = new File(rootDirectory + File.separator + 
						dirName + File.separator + fileName);
				try {
					if(file.exists())
					{
						BufferedReader reader = new BufferedReader(new InputStreamReader(
								new FileInputStream(file)));
						String line;
						while((line=reader.readLine())!=null){
							HashMap<String,Integer> values = bucket.get(line);
							if(values == null)
								values = new HashMap<String,Integer>();
							String oldValues = reader.readLine();
							if(oldValues!= null)
							{
								String indOldValues[] = oldValues.split(",");
								for(String oldValue : indOldValues)
								{
									//System.out.println(oldValue);
									String fileAndFreq[] = oldValue.split("-");
									String fileId = fileAndFreq[0];
									int freq = Integer.parseInt(fileAndFreq[1]);
									
									if(values.containsKey(fileId)){
										freq += values.get(fileId);
									}
									
									values.put(fileId,freq);
								}
							}
							bucket.put(line, values);
						}
						reader.close();
					}
					
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(file)));
					Set<Entry<String,HashMap<String,Integer>>> bucketSet = bucket.entrySet();
					Iterator<Entry<String,HashMap<String,Integer>>> iterator = bucketSet.iterator();
					while(iterator.hasNext()){
						Entry<String,HashMap<String,Integer>> entry = iterator.next();
						String key = entry.getKey();
						HashMap<String,Integer> values = entry.getValue();
						String valueString = "";
						//Iterator<String,Integer> valuesIter = values.iterator();
						Set<Entry<String,Integer>> valueSet = values.entrySet(); 
						Iterator<Entry<String,Integer>> valuesIter = valueSet.iterator();
						while(valuesIter.hasNext()){
							Entry<String,Integer> fileAndFreq = valuesIter.next();
							String fileId = fileAndFreq.getKey();
							String freq = fileAndFreq.getValue().toString();
							valueString = valueString + fileId + "-" + freq + ",";
						}
						writer.append(key);
						writer.newLine();
						writer.append(valueString);
						writer.newLine();
						//bucket.remove(key);
						//bucketSet = bucket.entrySet();
						//iterator = bucketSet.iterator();
					}
					writer.close();
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		//}
	}
}
