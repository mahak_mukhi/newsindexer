package edu.buffalo.cse.irf14.query;

import edu.buffalo.cse.irf14.index.IndexType;

public class Clause {
	
	IndexType index;
	String term;
	Query query;
	public IndexType getIndex() {
		return index;
	}
	public void setIndex(String index) {
		if(index == null){
			index =null;
			return;
		}
		if(index.equals("Author")){
			this.index = IndexType.AUTHOR;
			return;
		}
		if(index.equals("Term") || index.equals("none")){
			this.index = IndexType.TERM;
			return;
		}
		if(index.equals("Category")){
			this.index = IndexType.CATEGORY;
			return;
		}
		if(index.equals("Place")){
			this.index = IndexType.PLACE;
			return;
		}
		//this.index = index;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public Query getQuery() {
		return query;
	}
	public void setQuery(Query query) {
		this.query = query;
	}
	
	public String toString()
	{
		String toReturn;
		if(query==null)
		{
			toReturn = index+":"+term;
		}
		else{
			toReturn = query.toString();
			toReturn = toReturn.substring(2,toReturn.length()-2);
			toReturn = "[ " + toReturn + " ]";
		}
		return toReturn ;
	}

}
