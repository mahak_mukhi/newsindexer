package edu.buffalo.cse.irf14.query;

public enum Operator {
	AND,
	OR,
	NOT
}
