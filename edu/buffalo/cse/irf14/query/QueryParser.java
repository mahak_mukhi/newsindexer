/**
 * 
 */
package edu.buffalo.cse.irf14.query;

import java.util.Stack;


/**
 * @author nikhillo
 * Static parser that converts raw text to Query objects
 */
public class QueryParser {
	/**
	 * MEthod to parse the given user query into a Query object
	 * @param userQuery : The query to parse
	 * @param defaultOperator : The default operator to use, one amongst (AND|OR)
	 * @return Query object if successfully parsed, null otherwise
	 */
	public static Query parse(String userQuery, String defaultOperator) {
		//TODO: YOU MUST IMPLEMENT THIS METHOD
		Operator defOper = getOperator(defaultOperator);
		Stack<String> stack = new Stack<String>();
		Stack<String> indexStack = new Stack<String>();
		Query query = new Query();
		String words[] = userQuery.split(" +");
		// wee need to insert required parenthesis
		// for example in case of oranges apples peaches
		// it should be like (oranges apples peaches)
		for(int i=0;i<words.length-1;i++)
		{
			String word = words[i];
			if(!isOperator(word)&& !isOperator(words[i+1])){
				if(word.charAt(0)=='"')
				{
					int j;
					for(j=i;j<words.length;j++)
					{
						if(words[j].charAt(words[j].length()-1) == '"')
						{
							i=j+1;
							break;
						}
					}
					if(j==words.length){
						//throw new QueryParserException();
						return null;
					}
					continue;
				}
				if(word.charAt(0)=='('){
					int j;
					for(j=i;j<words.length;j++)
					{
						if(isOperator(words[j])){
							words[i] = "(" + words[i];
							words[j] = words[j-1] + ")";
							i=j+1;
							break;
						}
						
						if(words[j].charAt(words[j].length()-1) == ')')
						{
							i=j+1;
							break;
						}
					}
					if(j==words.length)
					{
						//throw new QueryParserException();
						return null;
					}
					continue;
				}
				int j;
				for(j=i+1;j<words.length;j++){
					if(isOperator(words[j]) || words[j].charAt(0)=='('){
						words[i] = "(" + words[i];
						words[j-1] = words[j-1] + ")";
						i=j+1;
						break;
					}
				}
				if(j==words.length){
					if(i!=0){
						words[i] = "(" + words[i];
						words[j-1] = words[j-1] + ")";
					}
					i=j;
				}
				
			}
		}
		int toStack = 0;
		boolean shouldBeOperator = false;
		//boolean isAutoQueryInitiated = false;
		for(int i=0;i<words.length;i++){
			String word = words[i];
			//word = word.trim();
			//System.out.println(words[i]);
			if(word.equals("AND") || word.equals("OR")
					|| word.equals("NOT"))
			{
				shouldBeOperator = false;
				if(toStack > 0)
				{
					stack.push(word);
					continue;
				}
				if(word.equals("AND"))
				{
					query.operators.add(Operator.AND);
					continue;
				}
				if(word.equals("OR"))
				{
					query.operators.add(Operator.OR);
					continue;
				}
				if(word.equals("NOT"))
				{
					query.operators.add(Operator.NOT);
					continue;
				}
				
			}else{
				if(shouldBeOperator){
					if(toStack>0)
					{
						switch(defOper){
						case AND:
							stack.push("AND");
							break;
						case NOT:
							stack.push("NOT");
							break;
						case OR:
							stack.push("OR");
							break;
						}
					}
					else{
						query.operators.add(defOper);
					}
				}
				else{
					shouldBeOperator = true;
				}
			}
			
			String index = null;
			int indexOfCol;
			if(word.charAt(0) != '('){//this may be a case like Author:(
				indexOfCol = word.indexOf(':');
				if(indexOfCol==-1)
				{
					index = "none";
				}
				else{
					String indexCand = word.substring(0,indexOfCol);
					if(indexCand.equalsIgnoreCase("Author")){
						index = "Author";
						word = word.substring(indexOfCol+1, word.length());
					}
					else if(indexCand.equalsIgnoreCase("Place")){
						index = "Place";
						word = word.substring(indexOfCol+1, word.length());
					}else if(indexCand.equalsIgnoreCase("Category")){
						index = "Category";
						word = word.substring(indexOfCol+1, word.length());
					}else if(indexCand.equalsIgnoreCase("Term")){
						index = "Term";
						word = word.substring(indexOfCol+1, word.length());
					}else{
						index = "none";
					}
				}
			}
			
			if(word.charAt(0)=='('){
				// this may be a case like (Author:dutt AND ...)
				//index = "none";
				// find the index
				// if none index use the one from stack
				// if that too is none assign term to it
				word = word.substring(1);
				String subIndex;
				int subIndexOfCol = word.indexOf(':');
				if(subIndexOfCol == -1){
					//String glIndex = indexStack.lastElement();
					subIndex = (index==null)?"none":index;
				}else{
					String subIndexCand = word.substring(0,subIndexOfCol);
					if(subIndexCand.equalsIgnoreCase("Author")){
						subIndex = "Author";
						word = word.substring(subIndexOfCol+1);
					}else if(subIndexCand.equalsIgnoreCase("Place")){
						subIndex = "Place";
						word = word.substring(subIndexOfCol+1);
					}else if(subIndexCand.equalsIgnoreCase("Category")){
						subIndex = "Category";
						word = word.substring(subIndexOfCol+1);
					}else if(subIndexCand.equalsIgnoreCase("Term")){
						subIndex = "Term";
						word = word.substring(subIndexOfCol+1);
					}else{
						//String glIndex = indexStack.lastElement();
						subIndex = (index==null)?"none":index;
					}
				}
				
				if(word.charAt(0)=='"'){
					
					int j;
					for(j=i+1;j<words.length;j++){
						word = word + " " + words[j];
						if(words[j].charAt(words[j].length()-1) == '"'
								|| words[j].charAt(words[j].length()-2) == '"')
						{
							i = j;
							break;
						}
					}
					if(j==words.length)
					{
						//throw new QueryParserException();
						return null;
					}
					word.replaceAll("\"", "");
				}
				
				if(word.charAt(word.length()-1) == ')')
				{
					Clause clause = new Clause();
					clause.setIndex(subIndex);
					clause.setQuery(null);
					clause.setTerm(word);
					query.clauses.add(clause);
				}else{
					toStack++;
					// nested query
					indexStack.push((index==null)?"none":index);
					stack.add("(");
					//indexStack
					stack.add(subIndex + ":" + word);
				}
				continue;
			}
			if(word.charAt(word.length()-1) == ')')
			{
				toStack--;
				// create a query object
				// create a clause object
				// add the query to clause
				// add this clause to the 
				// over all query object
				Query revSubQuery = new Query();
				String subIndex;
				if(index.equals("none"))
				{
					String glIndex = indexStack.lastElement();
					subIndex = (glIndex==null)?"none":glIndex;
				}
				else{
					subIndex = index;
				}
				Clause firstSubClause = new Clause();
				firstSubClause.setIndex(subIndex);
				firstSubClause.setTerm(word.substring(0,word.length()-1));
				firstSubClause.setQuery(null);
				revSubQuery.clauses.add(firstSubClause);
				String subClauseString;
				while(!(subClauseString = stack.pop()).equals("(")){
					if(subClauseString.equals("AND"))
					{
						revSubQuery.operators.add(Operator.AND);
					}else if(subClauseString.equals("OR")){
						revSubQuery.operators.add(Operator.OR);
					}else if(subClauseString.equals("NOT")){
						revSubQuery.operators.add(Operator.NOT);
					}
					else{
						// its a clause
						int subIndexOfCol = subClauseString.indexOf(":");
						String subClauseIndex = subClauseString.substring(0, subIndexOfCol);
						subClauseString = subClauseString.substring(subIndexOfCol+1);
						//System.out.println(subClauseString);
						Clause subClause = new Clause();
						subClause.setIndex(subClauseIndex);
						subClause.setTerm(subClauseString);
						subClause.setQuery(null);
						revSubQuery.clauses.add(subClause);
					}
				}
				indexStack.pop();
				Clause clause = new Clause();
				clause.setIndex(null);
				clause.setTerm(null);
				Query subQuery = new Query();
				for(int k=revSubQuery.clauses.size()-1;k>=0;k--){
					subQuery.clauses.add(revSubQuery.clauses.get(k));
				}
				for(int k=revSubQuery.operators.size()-1;k>=0;k--){
					subQuery.operators.add(revSubQuery.operators.get(k));
				}
				clause.setQuery(subQuery);
				query.clauses.add(clause);
				continue;
			}
			//Query autoQuery = null;
			if(word.charAt(0) == '"')
			{
				int j;
				for(j=i+1;j<words.length;j++){
					word = word + " " + words[j];
					if(words[j].charAt(words[j].length()-1) == '"')
					{
						//System.out.println("never here");
						i = j;
						break;
					}
				}
				if(j==words.length)
				{
					//throw new QueryParserException();
					return null;
				}
				word = word.substring(1, word.length()-1);
			}
			
			if(toStack!=0){
				//add to stack
				if(index == "none"){
					index = indexStack.lastElement();
				}
				stack.add(index+":"+word);
			}
			else{
				//index = indexStack.pop();
				Clause clause = new Clause();
				clause.setIndex(index);
				clause.term = word;
				clause.query = null;
				query.clauses.add(clause);
				
			}
		}
		return query;
	}
	
	 private static Operator getOperator(String operatorString){
		if(operatorString.equals("AND"))
		{
			return Operator.AND;
		}
		else if(operatorString.equals("OR"))
		{
			return Operator.OR;
		}
		else if(operatorString.equals("NOT"))
		{
			return Operator.NOT;
		}
		else{
			return null;
		}
	}
	
	private static boolean isOperator(String string)
	{
		if(string.equals("AND")||string.equals("OR")||string.equals("NOT"))
		{
			return true;
		}
		return false;
	}
}
