package edu.buffalo.cse.irf14.query;

import java.util.ArrayList;


/**
 * Class that represents a parsed query
 * @author nikhillo
 *
 */
public class Query {
	ArrayList<Clause> clauses = new ArrayList<Clause>();
	ArrayList<Operator> operators = new ArrayList<Operator>();
	public ArrayList<Clause> getClauses() {
		return clauses;
	}

	//public void setClauses(ArrayList<Clause> clauses) {
		//this.clauses = clauses;
	//}

	public ArrayList<Operator> getOperators() {
		return operators;
	}

	//public void setOperators(ArrayList<Operator> operators) {
		//this.operators = operators;
	//}

	/**
	 * Method to convert given parsed query into string
	 */
	public String toString() {
		//TODO: YOU MUST IMPLEMENT THIS
		if (operators.size() < clauses.size()) {
			String toReturn = "{";
			boolean isNot = false;
			for (int i = 0; i < operators.size(); i++) {
				if (isNot) {
					isNot = false;
					toReturn += " <" + clauses.get(i).toString() + "> "
							+ getOperatorString(operators.get(i));
				} else {
					toReturn += " " + clauses.get(i).toString() + " "
							+ getOperatorString(operators.get(i));
				}
				if (operators.get(i) == Operator.NOT) {
					// System.out.println(true);
					isNot = true;
				}
			}
			if (isNot) {
				toReturn += " <" + clauses.get(clauses.size() - 1) + "> }";
			} else {
				toReturn += " " + clauses.get(clauses.size() - 1) + " }";
			}
			return toReturn;
		} else {
			String toReturn = "{";
			//boolean isNot = false;
			for (int i = 0; i < operators.size(); i++) {
				//if (isNot) {
					//isNot = false;
					//toReturn += " " + getOperatorString(operators.get(i))
						//	+ " <" + clauses.get(i).toString() + ">";
				//} else {
					//toReturn += " " + getOperatorString(operators.get(i)) 
						//	+ " " + clauses.get(i).toString() ;
				//}
				if (operators.get(i) == Operator.NOT) {
					// System.out.println(true);
					//isNot = true;
					if(i==0){
						toReturn += " <" + clauses.get(i).toString() + ">";
					}else{
						toReturn += " " + getOperatorString(operators.get(i))
									+ " <" + clauses.get(i).toString() + ">";
					}
					
				}else{
					toReturn += " " + getOperatorString(operators.get(i)) 
								+ " " + clauses.get(i).toString() ;
				}
			}
			//if (isNot) {
				//toReturn += " <" + clauses.get(clauses.size() - 1) + "> }";
			//} else {
				//toReturn += " " + clauses.get(clauses.size() - 1) + " }";
			//}
			return toReturn;
		}
	}
	
	private String getOperatorString(Operator oper)
	{
		switch(oper){
		case AND:
			return "AND";
		case NOT:
			return "AND";
		case OR:
			return "OR";
		default:
			return null;
		
		}
	}
}
